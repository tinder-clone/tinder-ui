import React , {useState} from 'react';
import TinderCard from 'react-tinder-card';
import "./TinderCards.css";


function TinderCards() {
    const [people] = useState([
        {
            name:"Elon Musk",
            url:"https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.DjyPSeO-t7bK2agoLSYJzgHaHa%26pid%3DApi&f=1"
        },
        {
            name:"Tom Hanks",
            url:"https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.UmpIfmbBikCqWsTEI_vboAHaKt%26pid%3DApi&f=1"
        }
    ]);
    const swiped = (dir, nameToDelete)=>{
        console.log("removing "+nameToDelete);

    }
    const outOfFrame = (name)=>{
        console.log(name + " left the screen")
    }

    return(<div className="tinderCards">
            {people.map((person)=>{
                return (<TinderCard
                className="swipe"
                key={person.name}
                preventSwipe={["up","down"]}
                onSwipe={(dir)=>{swiped(dir, person.name)}}
                onCardLeftScreen={()=>outOfFrame(person.name)}
                >
                    <div
                    style={{backgroundImage: `url(${person.url})`}}
                    className="card"
                    >
                        <h3>{person.name}</h3>
                    </div>
                </TinderCard>);

            })}
        </div>);
}

export default TinderCards;
